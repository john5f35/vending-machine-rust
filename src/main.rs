use std::fs;
use std::convert::{ TryInto, TryFrom };
use structopt::StructOpt;
use unindent::unindent;
use rustyline::Editor;
use prettytable::{Table, Row, row, cell, format};
use regex::Regex;
use anyhow::{ Result, Error };

mod model;
use model::{ Merchandise, Currency, KeyCode, VendingMachine };


#[derive(StructOpt, Debug)]
#[structopt(name = "vmrepl", about="Vending Machine REPL")]
struct Opt {
    /// Save the machine state back to JSON
    #[structopt(short, long, )]
    save: bool,

    /// State JSON file
    #[structopt(name="FILE")]
    state_file: String,
}

fn print_help_text() -> () {
    println!("{}", unindent(format!("\
        Vending Machine REPL

        - Type 'list' to list all available merchandises;
        - Type '$<amount>' to insert money (e.g. $0.2 to insert 20 cents)
            Accepted currencies are: {accepted_currency:?}
        - Type '<keycode>' to buy desired item;
        - Type 'help' to show this help text;
        - '<Ctl-D>' to exit", accepted_currency=Currency::ACCEPTED_CURRENCY).as_str()).as_str());
}

fn print_tabulated<'a, I>(items: I) -> ()
where I: Iterator<Item = (&'a KeyCode, &'a Merchandise)> {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
    table.set_titles(row!["keycode", "name", "price"]);

    items.for_each(| (keycode, merch) | {
        table.add_row(row![&*keycode, &merch.name, format!("{}", merch.price).as_str()]);
    });

    table.printstd();
}

fn tabprint(items: &[&str], header: &[&str]) -> () {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_NO_LINESEP_WITH_TITLE);
    table.set_titles(Row::from(header));
    for x in items {
        
    }
    table.printstd();
}

fn process_input(vm: &mut VendingMachine, input: &str) {
    match input {
        "list" => {
            print_tabulated(vm.merch_map.iter().filter(|(_, merch)| { merch.stock > 0 }));
        },
        amount if amount.starts_with("$") => {
            if let Ok(flt) = amount[1..].parse::<f32>() {
                if let Ok(c) = flt.try_into() {
                    vm.insert_currency(c);
                    println!("Session credit: {}", vm.session_credit);
                    return
                }
            }
            println!("Invalid input: {}", amount);
        }
        "help" => { print_help_text(); },
        _str => {
            if let Ok(keycode) = KeyCode::try_from(&_str.to_string()) {
                if vm.merch_map.contains_key(&keycode) {
                    match vm.buy(&keycode) {
                        Ok((item, change)) => {
                            println!("You bought a '{}'!", item);
                            if change.len() > 0 {
                                // print_tabulated(change.iter());
                            }
                        }
                        Err(err) => { println!("{}", err); }
                    }
                }
            } else {
                println!("Invalid input: {}", _str);
            }
        }
    };
}

fn main() -> Result<()> {
    let opt: Opt = Opt::from_args();

    let contents = fs::read_to_string(&opt.state_file).expect(&format!("Failed to read file {:?}", opt.state_file));
    // let mut vm = VendingMachine::from_json(&contents)?;
    let mut vm= contents.try_into()?;

    print_help_text();

    let mut rl = Editor::<()>::new();
    let mut session_alive = true;
    while session_alive {
        session_alive = match rl.readline("(Vending Machine REPL)>> ") {
            Ok(input) => { process_input(&mut vm, input.as_str()); true }
            Err(_) => false
        }
    }

    Ok(())
}


#[cfg(test)]
mod tests {
    use crate::tabprint;

    #[test]
    fn test_tabprint() {
        tabprint(&["1", "2", "3"], &["a", "b", "c"]);
    }
}
