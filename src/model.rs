use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use float_cmp::{approx_eq};
use regex::Regex;
use std::convert::TryFrom;
use std::ops;
use std::hash::{Hash, Hasher};
use serde_json::{Value, Map};
// use anyhow::{ anyhow, ensure, bail };
use anyhow::{Result, Error};
use anyhow::anyhow as err;
use std::cmp::{max, min};
use maplit::hashmap;

type Money = HashMap<Currency, u16>;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Merchandise {
    pub name: String,
    pub price: f32,
    pub stock: u8,
}

impl PartialEq for Merchandise {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name && approx_eq!(f32, self.price, other.price) && self.stock == other.stock
    }
}

impl TryFrom<&Value> for Merchandise {
    type Error = Error;
    fn try_from(value: &Value) -> Result<Self> {
        let name = value["name"].as_str().map(&str::to_string).ok_or(err!("parse error!"))?;
        let price = value["price"].as_f64().map(|f| f as f32).ok_or(err!("parse error!"))?;
        let stock = value["stock"].as_i64().map(|i| i as u8).ok_or(err!("parse error!"))?;
        Ok(Merchandise { name, price, stock })
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, Hash)]
pub struct KeyCode(String);

impl ops::Deref for KeyCode {
    type Target = String;
    fn deref(&self) -> &Self::Target { &self.0 }
}

impl PartialEq for KeyCode {
    fn eq(&self, other: &Self) -> bool { self.0 == other.0 }
}

impl TryFrom<&String> for KeyCode {
    type Error = Error;
    fn try_from(value: &String) -> Result<Self> {
        if !Regex::new(r"^[1-6][1-9]$")?.is_match(value.as_str()) {
            Err(err!("Invalid KeyCode format."))
        } else {
            Ok(KeyCode(value.into()))
        }
    }
}


#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Currency(f32);

impl Currency {
    pub const ACCEPTED_CURRENCY: [f32; 6] = [0.2, 0.5, 1.0, 2.0, 5.0, 10.0];
}

impl ops::Deref for Currency {
    type Target = f32;
    fn deref(&self) -> &Self::Target { &self.0 }
}

impl PartialEq for Currency {
    fn eq(&self, other: &Self) -> bool { approx_eq!(f32, self.0, other.0) }
}

impl Eq for Currency {}

impl Hash for Currency {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.to_be_bytes().hash(state);
    }
}

impl TryFrom<f32> for Currency {
    type Error = Error;
    fn try_from(value: f32) -> Result<Self> {
        if !Currency::ACCEPTED_CURRENCY.contains(&value) {
            Err(err!("Currency of value {} is not accepted.", value))
        } else {
            Ok(Currency(value))
        }
    }
}


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct VendingMachine {
    pub merch_map: HashMap<KeyCode, Merchandise>,
    pub money_reserve: Money,

    #[serde(skip_serializing, default = "VendingMachine::default_session_credit")]
    pub session_credit: f32,
}

impl TryFrom<String> for VendingMachine {
    type Error = Error;
    fn try_from(json_str: String) -> Result<Self> {
        serde_json::from_str::<Value>(json_str.as_str())?.as_object().and_then(|obj| {
            let merch_map = obj["merch_map"].as_object().and_then(|_map: &Map<String, Value>| {
                _map.iter().map(|(keystr, _value)| {
                    let keycode = KeyCode::try_from(keystr).ok()?;
                    let merch = Merchandise::try_from(_value).ok()?;
                    Some((keycode, merch))
                }).collect::<Option<HashMap<KeyCode, Merchandise>>>()
            })?;
            let money_reserve = obj["money_reserve"].as_object().and_then(|_mr_map| {
                _mr_map.iter().map(|(cur_str, _value)| {
                    let currency = Currency::try_from(cur_str.parse::<f32>().ok()?).ok()?;
                    let count = _value.as_i64().map(|i| i as u16)?;
                    Some((currency, count))
                }).collect::<Option<Money>>()
            })?;
            Some(VendingMachine {
                merch_map,
                money_reserve,
                session_credit: VendingMachine::default_session_credit(),
            })
        }).ok_or(err!("Unexpected JSON format!"))
    }
}

impl VendingMachine {
    fn default_session_credit() -> f32 { 0.0 }

    pub fn insert_currency(&mut self, c: Currency) { self.session_credit += c.0; }

    pub fn buy(&mut self, keycode: &KeyCode) -> Result<(String, Money)> {
        if ! self.merch_map.contains_key(keycode) {
            return Err(err!("Nothing at slot {:#?}", keycode));
        }
        let amount: f32;
        {
            let merch = self.merch_map.get(keycode).expect("Check contains but failed to get?");
            if self.session_credit < merch.price {
                return Err(err!("Insufficient fund!"));
            }
            amount = self.session_credit - merch.price;
        }
        let (shadow_reserve, change) = split_change(&self.money_reserve, amount)
            .ok_or(err!("Cannot split change {} with money storage.", amount))?;
        let merch_mut = self.merch_map.get_mut(keycode).expect("Check contains but failed to get?");
        merch_mut.stock -= 1;
        self.session_credit -= merch_mut.price;
        self.money_reserve = shadow_reserve;
        Ok((merch_mut.name.clone(), change))
    }

}

fn split_change(money_reserve: &Money, amount: f32) -> Option<(Money, Money)> {
    let mut _amount = amount;
    let mut shadow_reserve = money_reserve.clone();
    let mut change = Money::new();
    let mut sorted_currency: Vec<Currency> = shadow_reserve.keys().copied().collect();
    sorted_currency.sort_by(|x, y| x.partial_cmp(&y).expect("Currencies should be comparable"));
    sorted_currency.reverse();
    for currency in sorted_currency {
        let count = (_amount / currency.0) as u16;
        let reserve = *shadow_reserve.get(&currency)?;
        let take = min(count, reserve);
        if take > 0u16 {
            println!("amount: {:?}, take: {:?}, currency: {:?}", _amount, take, currency);
            shadow_reserve.entry(currency.clone()).and_modify(|c| *c -= take);
            _amount -= currency.0 * take as f32;
            change.insert(currency, take);
        }
    }

    if approx_eq!(f32, _amount, 0.0) { Some((shadow_reserve, change)) } else { None }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryInto;
    use std::io::SeekFrom::Current;
    use std::ptr::hash;

    #[test]
    fn test_from_json() -> Result<()> {
        let json_str = r#"{
            "merch_map": {
                "12": { "name": "Item", "price": 3.50, "stock": 2 }
            },
            "money_reserve": { "0.2": 10 }
        }
        "#;
        let vm = VendingMachine::try_from(json_str.to_string())?;
        assert_eq!(vm.money_reserve, [(Currency(0.2), 10)].iter().cloned().collect());
        assert_eq!(vm.merch_map, [(KeyCode("12".to_string()), Merchandise { name: "Item".to_string(), price: 3.50, stock: 2 })].iter().cloned().collect());
        assert!(approx_eq!(f32, vm.session_credit, 0.0));
        Ok(())
    }

    #[test]
    fn test_split_change() -> Result<()> {
        let money_reserve: Money = hashmap! {
            Currency(0.5) => 3,
            Currency(1.0) => 2,
            Currency(2.0) => 1,
        };

        assert_eq!(split_change(&money_reserve, 0.2), None);
        assert_eq!(split_change(&money_reserve, 0.7), None);

        let (shadow_reserve, change) = split_change(&money_reserve, 3.5).expect("No error");
        println!("shadow_reserve: {:?}", shadow_reserve);
        println!("change: {:?}", change);
        assert_eq!(shadow_reserve, hashmap! {
            Currency(0.5) => 2,
            Currency(1.0) => 1,
            Currency(2.0) => 0
        });
        assert_eq!(change, hashmap! {
            Currency(0.5) => 1,
            Currency(1.0) => 1,
            Currency(2.0) => 1
        });

        assert_eq!(split_change(&shadow_reserve, 3.0), None);
        Ok(())
    }

    #[test]
    fn test_buy_merch() -> Result<()> {
        let mut vm = VendingMachine {
            merch_map: hashmap! {
                KeyCode("12".to_string()) => Merchandise { name: "Item".to_string(), price: 1.5, stock: 2 },
                KeyCode("23".to_string()) => Merchandise { name: "Another".to_string(), price: 1.5, stock: 2},
            },
            session_credit: 2.0,
            money_reserve: hashmap! {
                Currency(0.5) => 3,
                Currency(1.0) => 2,
                Currency(2.0) => 1,
            }
        };
        let (item_name, change) = vm.buy(&KeyCode("12".to_string()))?;
        assert_eq!(item_name, "Item".to_string());
        assert_eq!(change, hashmap! {
            Currency(0.5) => 1
        });
        Ok(())
    }
}